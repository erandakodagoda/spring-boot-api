package com.api.controllers;

import com.api.models.DiscountCode;
import com.api.repositories.DiscountCodeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@RestController
@RequestMapping("/api/v1/discount-codes")
public class DiscountCodeController {
    @Autowired
    private DiscountCodeRepository discountCodeRepository;

    @GetMapping
    public List<DiscountCode> list(){
        return discountCodeRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public DiscountCode get(@PathVariable Long id){
        return discountCodeRepository.getOne(id);
    }

    @PostMapping
    public DiscountCode create(@RequestBody final DiscountCode discountCode){
        return discountCodeRepository.saveAndFlush(discountCode);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Long id){
        discountCodeRepository.deleteById(id);
        return "Discount code id "+id+" has been deleted successfully!";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public DiscountCode update(@PathVariable Long id, @RequestBody DiscountCode discountCode){
        DiscountCode existingDiscountCode = discountCodeRepository.getOne(id);
        BeanUtils.copyProperties(discountCode, existingDiscountCode, "discount_code_id");
        return discountCodeRepository.saveAndFlush(discountCode);
    }
}

