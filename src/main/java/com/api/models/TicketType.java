package com.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@Entity(name = "ticket_types")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TicketType {

    @Id
    private String ticket_type_code;
    private String ticket_type_name;
    private String description;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean includes_workshop;

    public String getTicket_type_code() {
        return ticket_type_code;
    }

    public void setTicket_type_code(String ticket_type_code) {
        this.ticket_type_code = ticket_type_code;
    }

    public String getTicket_type_name() {
        return ticket_type_name;
    }

    public void setTicket_type_name(String ticket_type_name) {
        this.ticket_type_name = ticket_type_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isIncludes_workshop() {
        return includes_workshop;
    }

    public void setIncludes_workshop(boolean includes_workshop) {
        this.includes_workshop = includes_workshop;
    }
}
