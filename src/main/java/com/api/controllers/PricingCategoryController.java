package com.api.controllers;

import com.api.models.PricingCategory;
import com.api.repositories.PricingCategoryRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@RestController
@RequestMapping("/api/v1/pricing-categories")
public class PricingCategoryController {
    @Autowired
    private PricingCategoryRepository pricingCategoryRepository;

    @GetMapping
    public List<PricingCategory> list(){
        return pricingCategoryRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{code}")
    public PricingCategory get(@PathVariable String code){
        return pricingCategoryRepository.getOne(code);
    }

    @PostMapping
    public PricingCategory create(@RequestBody final PricingCategory pricingCategory){
        return pricingCategoryRepository.saveAndFlush(pricingCategory);
    }
    @RequestMapping(value = "{code}", method = RequestMethod.DELETE)
    public String delete(@PathVariable String code){
        pricingCategoryRepository.deleteById(code);
        return "Pricing Category Code "+code+" deleted successfully!";
    }
    @RequestMapping(value = "{code}", method = RequestMethod.PUT)
    public PricingCategory update(@PathVariable String code, @RequestBody PricingCategory pricingCategory){
        PricingCategory existingCategory = pricingCategoryRepository.getOne(code);
        BeanUtils.copyProperties(pricingCategory, existingCategory, "pricing_category_code");
        return pricingCategoryRepository.saveAndFlush(pricingCategory);
    }

}
