package com.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@Entity(name = "discount_codes")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DiscountCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long discount_code_id;
    private String discount_name;
    private String discount_code;
    private String discount_type;
    private Double discount_amount;

    public Long getDiscount_code_id() {
        return discount_code_id;
    }

    public void setDiscount_code_id(Long discount_code_id) {
        this.discount_code_id = discount_code_id;
    }

    public String getDiscount_name() {
        return discount_name;
    }

    public void setDiscount_name(String discount_name) {
        this.discount_name = discount_name;
    }

    public String getDiscount_code() {
        return discount_code;
    }

    public void setDiscount_code(String discount_code) {
        this.discount_code = discount_code;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public Double getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(Double discount_amount) {
        this.discount_amount = discount_amount;
    }
}
