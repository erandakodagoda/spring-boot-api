package com.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@Entity(name = "attendee_tickets")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AttendeeTicket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long attendee_ticket_id;

    @ManyToOne
    @JoinColumn(name = "attendee_id")
    private Attendee attendee_id;
    @ManyToOne
    @JoinColumn(name = "ticket_price_id")
    private TicketPrice ticket_price_id;
    @ManyToOne
    @JoinColumn(name = "discount_code_id")
    private DiscountCode discount_code_id;
    private Double net_price;

    public Long getAttendee_ticket_id() {
        return attendee_ticket_id;
    }

    public void setAttendee_ticket_id(Long attendee_ticket_id) {
        this.attendee_ticket_id = attendee_ticket_id;
    }

    public Attendee getAttendee_id() {
        return attendee_id;
    }
    @JsonProperty("attendee_id")
    public void setAttendee_id(Integer attendee_id) {
        this.attendee_id = new Attendee();
        Long attendID = new Long(attendee_id);
        this.attendee_id.setAttendee_id(attendID);
    }

    public TicketPrice getTicket_price_id() {
        return ticket_price_id;
    }
    @JsonProperty("ticket_price_id")
    public void setTicket_price_id(Integer ticket_price_id) {
        this.ticket_price_id = new TicketPrice();
        Long ticketID = new Long(ticket_price_id);
        this.ticket_price_id.setTicket_price_id(ticketID);
    }

    public DiscountCode getDiscount_code_id() {
        return discount_code_id;
    }
    @JsonProperty("discount_code_id")
    public void setDiscount_code_id(Integer discount_code_id) {
        this.discount_code_id = new DiscountCode();
        Long discountID = new Long(discount_code_id);
        this.discount_code_id.setDiscount_code_id(discountID);
    }

    public Double getNet_price() {
        return net_price;
    }

    public void setNet_price(Double net_price) {
        this.net_price = net_price;
    }
}
