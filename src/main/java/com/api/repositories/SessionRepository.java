package com.api.repositories;

import com.api.models.Session;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 12, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */


public interface SessionRepository extends JpaRepository<Session, Long>{
}
