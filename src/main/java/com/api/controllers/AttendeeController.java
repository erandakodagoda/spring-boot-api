package com.api.controllers;

import com.api.models.Attendee;
import com.api.repositories.AttendeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@RestController
@RequestMapping("/api/v1/attendees")
public class AttendeeController {
    @Autowired
    private AttendeeRepository attendeeRepository;

    @GetMapping
    public List<Attendee> list(){
        return attendeeRepository.findAll();
    }
    @GetMapping
    @RequestMapping("{id}")
    public Attendee get(@PathVariable Long id){
        return attendeeRepository.getOne(id);
    }
    @PostMapping
    public Attendee create(@RequestBody final Attendee attendee){
        return attendeeRepository.saveAndFlush(attendee);
    }
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Long id){
        attendeeRepository.deleteById(id);
        return "Attendee id "+id+" has been deleted successfully!";
    }
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public Attendee update(@PathVariable Long id, @RequestBody Attendee attendee){
        Attendee existingAttendee = attendeeRepository.getOne(id);
        BeanUtils.copyProperties(attendee, existingAttendee, "attendee_id");
        return attendeeRepository.saveAndFlush(attendee);
    }
}
