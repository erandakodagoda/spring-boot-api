package com.api.repositories;

import com.api.models.Speaker;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 12, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

public interface SpeakerRepository extends JpaRepository<Speaker, Long>{
}
