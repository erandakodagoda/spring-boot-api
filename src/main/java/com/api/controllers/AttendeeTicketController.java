package com.api.controllers;

import com.api.models.AttendeeTicket;
import com.api.repositories.AttendeeTicketRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@RestController
@RequestMapping("/api/v1/attendee-tickets")
public class AttendeeTicketController {
    @Autowired
    private AttendeeTicketRepository attendeeTicketRepository;

    @GetMapping
    public List<AttendeeTicket> list(){
        return attendeeTicketRepository.findAll();
    }
    @GetMapping
    @RequestMapping("{id}")
    public AttendeeTicket get(@PathVariable Long id){
        return attendeeTicketRepository.getOne(id);
    }
    @PostMapping
    public AttendeeTicket create(@RequestBody final AttendeeTicket attendeeTicket){
        return attendeeTicketRepository.saveAndFlush(attendeeTicket);
    }
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Long id){
        attendeeTicketRepository.deleteById(id);
        return "Attendee Ticket id "+id+" has been deleted successfully!";
    }
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public AttendeeTicket update(@PathVariable Long id, @RequestBody AttendeeTicket attendeeTicket){
        AttendeeTicket existingTicket = attendeeTicketRepository.getOne(id);
        BeanUtils.copyProperties(attendeeTicket, existingTicket, "attendee_ticket_id");
        return attendeeTicketRepository.saveAndFlush(attendeeTicket);
    }
}
