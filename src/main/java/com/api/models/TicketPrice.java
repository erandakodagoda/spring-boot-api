package com.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@Entity(name = "ticket_prices")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TicketPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ticket_price_id;
    @ManyToOne
    @JoinColumn(name = "ticket_type_code")
    private TicketType ticket_type_code;
    @ManyToOne
    @JoinColumn(name = "pricing_category_code")
    private PricingCategory pricing_category_code;
    private Double base_price;

    public Long getTicket_price_id() {
        return ticket_price_id;
    }

    public void setTicket_price_id(Long ticket_price_id) {
        this.ticket_price_id = ticket_price_id;
    }

    public TicketType getTicket_type_code() {
        return ticket_type_code;
    }
    @JsonProperty("ticket_type_code")
    public void setTicket_type_code(String ticket_type_code) {
        this.ticket_type_code = new TicketType();
        this.ticket_type_code.setTicket_type_code(ticket_type_code);
    }

    public PricingCategory getPricing_category_code() {
        return pricing_category_code;
    }
    @JsonProperty("pricing_category_code")
    public void setPricing_category_code(String pricing_category_code) {
        this.pricing_category_code = new PricingCategory();
        this.pricing_category_code.setPricing_category_code(pricing_category_code);
    }

    public Double getBase_price() {
        return base_price;
    }

    public void setBase_price(Double base_price) {
        this.base_price = base_price;
    }
}
