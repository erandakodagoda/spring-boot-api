package com.api.repositories;

import com.api.models.DiscountCode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

public interface DiscountCodeRepository  extends JpaRepository<DiscountCode, Long> {
}
