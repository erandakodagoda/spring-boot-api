package com.api.controllers;

import com.api.models.TicketPrice;
import com.api.repositories.TicketPriceRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@RestController
@RequestMapping("/api/v1/ticket-prices")
public class TicketPriceController {
    @Autowired
    private TicketPriceRepository ticketPriceRepository;

    @GetMapping
    public List<TicketPrice> list(){
        return ticketPriceRepository.findAll();
    }
    @GetMapping
    @RequestMapping("{id}")
    public TicketPrice get(@PathVariable Long id){
        return ticketPriceRepository.getOne(id);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TicketPrice create(@RequestBody final TicketPrice ticketPrice){
        return ticketPriceRepository.saveAndFlush(ticketPrice);
    }
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Long id){
        ticketPriceRepository.deleteById(id);
        return "Ticket Record "+ id +" Deleted Successfully";
    }
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public TicketPrice update(@PathVariable Long id, @RequestBody TicketPrice ticketPrice){
        TicketPrice existingTicketPrice = ticketPriceRepository.getOne(id);
        BeanUtils.copyProperties(ticketPrice, existingTicketPrice, "ticket_price_id");
        return ticketPriceRepository.saveAndFlush(ticketPrice);
    }
}
