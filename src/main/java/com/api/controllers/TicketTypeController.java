package com.api.controllers;

import com.api.models.TicketType;
import com.api.repositories.TicketTypeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  @author : Eranda Kodagoda
 *  @date : October 13, 2020
 *  @version : 1.0
 *  @copyright : © 2020 Eranda Kodagoda
 *  */

@RestController
@RequestMapping("/api/v1/ticket-types")
public class TicketTypeController {
    @Autowired
    private TicketTypeRepository ticketTypeRepository;

    @GetMapping
    public List<TicketType> list(){
        return ticketTypeRepository.findAll();
    }
    @GetMapping
    @RequestMapping("{code}")
    public TicketType get(@PathVariable String code){
        return ticketTypeRepository.getOne(code);
    }
    @PostMapping
    public TicketType create(@RequestBody final TicketType ticketType){
        return ticketTypeRepository.saveAndFlush(ticketType);
    }
    @RequestMapping(value = "{code}", method = RequestMethod.DELETE)
    public String delete(@PathVariable String code){
        ticketTypeRepository.deleteById(code);
        return "Ticket Type Code "+code+" deleted successfully!";
    }
    @RequestMapping(value = "{code}", method = RequestMethod.PUT)
    public TicketType update(@PathVariable String code, @RequestBody TicketType ticketType){
        TicketType existingTicketType = ticketTypeRepository.getOne(code);
        BeanUtils.copyProperties(ticketType, existingTicketType,"ticket_type_code");
        return ticketTypeRepository.saveAndFlush(ticketType);
    }
}
